
var app = {
    get: function(id) {
        return document.getElementById(id);
    },
    onload: function(func) {
        // http://www.dustindiaz.com/top-ten-javascript/
        var oldonload = window.onload;
        if (typeof window.onload !== 'function') {
            window.onload = func;
        } else {
            window.onload = function() {
                if (oldonload) {
                    oldonload();
                }
                func();
            };
        }
    },
    onunload: function(func) {
        // based on above
        var oldonunload = window.onunload;
        if (typeof window.onunload !== 'function') {
            window.onunload = func;
        } else {
            window.onunload = function() {
                if (oldonunload) {
                    oldonunload();
                }
                func();
            };
        }
    }
};

